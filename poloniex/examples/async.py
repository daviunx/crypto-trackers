from poloniex.app import AsyncApp
import json
from datetime import datetime
from elasticsearch import Elasticsearch
import certifi

es = Elasticsearch(
    ['https://search-xchanes-6m2rziqwlo3bxesksaixlg4fee.us-west-2.es.amazonaws.com/'],
    # http_auth=('user', 'secret'),
    port=443,
    use_ssl=True
)

indexName = "stock-"+datetime.now().strftime("%Y.%m.%d")
if not es.indices.exists(indexName):
    mapping = '''
    {
        "settings" : {
            "index" : {
                "number_of_shards" : 2,
                "number_of_replicas" : 1
            }
        },
        "mappings": {
            "ticker":{
              "properties":{
                "@timestamp":{
                  "type":"date",
                  "format": "epoch_millis"
                }
              }
            },
            "trades":{
              "properties":{
                "@timestamp":{
                  "type":"date",
                  "format" : "epoch_millis"
                 }
              }
            }
        }
    }'''
    es.indices.create(index=indexName, body=mapping)

__author__ = 'andrew.shvv@gmail.com'

API_KEY = "YOUR_API_KEY"
API_SECRET = "YOUR_API_SECRET"


class App(AsyncApp):
    def ticker(self, **kwargs):
        document = self.prepare_document(kwargs)
        kwargs['@timestamp'] = int(datetime.now().timestamp() * 1000)
        kwargs['provider'] = 'poloniex'
        print(json.dumps(document))
        res = es.index(index=indexName, doc_type='ticker', body=json.dumps(document))

    def trades(self, **kwargs):
        kwargs['@timestamp'] = int(datetime.now().timestamp() * 1000)
        kwargs['provider'] = 'poloniex'
        kwargs['data'] = self.prepare_document(kwargs['data'])
        print(json.dumps(kwargs))
        res = es.index(index=indexName, doc_type='trades', body=json.dumps(kwargs))

    def num(self, s):
        try:
            return int(s)
        except ValueError:
            return float(s)

    def prepare_document(self, kwargs):
        for key, value in kwargs.items():
            try:
                kwargs[key] = self.num(value)
            except ValueError:
                kwargs[key] = value

        return kwargs

    async def main(self):
        self.push.subscribe(topic="BTC_XRP", handler=self.trades)
        self.push.subscribe(topic="BTC_STR", handler=self.trades)
        self.push.subscribe(topic="BTC_ETH", handler=self.trades)

        self.push.subscribe(topic="ticker", handler=self.ticker)
        # volume = await self.public.return24hVolume()
        # self.logger.info(volume)

app = App(api_key=API_KEY, api_sec=API_SECRET)
app.run()
